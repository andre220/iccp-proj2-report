% ****** Start of file apssamp.tex ******
%
%   This file is part of the APS files in the REVTeX 4.1 distribution.
%   Version 4.1r of REVTeX, August 2010
%
%   Copyright (c) 2009, 2010 The American Physical Society.
%
%   See the REVTeX 4 README file for restrictions and more information.
%
% TeX'ing this file requires that you have AMS-LaTeX 2.0 installed
% as well as the rest of the prerequisites for REVTeX 4.1
%
% See the REVTeX 4 README file
% It also requires running BibTeX. The commands are as follows:
%
%  1)  latex apssamp.tex
%  2)  bibtex apssamp
%  3)  latex apssamp.tex
%  4)  latex apssamp.tex
%
\documentclass[%
 reprint,
%superscriptaddress,
%groupedaddress,
%unsortedaddress,
%runinaddress,
%frontmatterverbose, 
%preprint,
%showpacs,preprintnumbers,
%nofootinbib,
%nobibnotes,
%bibnotes,
 amsmath,amssymb,
 aps,
%pra,
%prb,
%rmp,
%prstab,
%prstper,
%floatfix,
]{revtex4-1}

\usepackage{graphicx}% Include figure files
\usepackage{dcolumn}% Align table columns on decimal point
\usepackage{bm}% bold math
%\usepackage{hyperref}% add hypertext capabilities
%\usepackage[mathlines]{lineno}% Enable numbering of text and display math
%\linenumbers\relax % Commence numbering lines

%\usepackage[showframe,%Uncomment any one of the following lines to test 
%%scale=0.7, marginratio={1:1, 2:3}, ignoreall,% default settings
%%text={7in,10in},centering,
%%margin=1.5in,
%%total={6.5in,8.75in}, top=1.2in, left=0.9in, includefoot,
%%height=10in,a5paper,hmargin={3cm,0.8in},
%]{geometry}

\begin{document}

\preprint{APS/123-QED}

\title{Variational Quantum Monte Carlo of the Hydrogen Molecule}% Force line breaks with \\
%\thanks{International Course on Computation Physics (ICCP)}%

\author{Kraig J. Andrews}
 %\email{andre220@msu.edu}
\affiliation{%
 Physics Department, Michigan State University\\
}%
\date{\today}% It is always \today, today,
             %  but any date may be explicitly specified

\begin{abstract}
An article usually includes an abstract, a concise summary of the work
covered at length in the main body of the article. 
\end{abstract}

\maketitle

%\tableofcontents

\section{Introduction}\label{sec:intro}
Over the past few decades increases in computing power have allowed for improved simulations of increasingly complex system. This development has allowed scientists to test theories and produce data. The Quantum Monte Carlo (QMC) is a large class of computer algorithms that simulate quantum systems with the idea of solving the quantum many-body problem. Though variations on the way that the method is employed exist, all of them use the Monte Carlo method to compute the $N$-dimensional integral that arise. The Schro\"{o}dinger equation can describe the physical system and through the use of Monte Carlo methods one can compute the ground-state energy at zero temperature (more complex methods do exist to compute quantities for finite temperature systems, i.e. path integral Monte Carlo and auxiliary field Monte Carlo \cite{Herman82, Ceperley95}). 

The main methods in use for the Monte Carlo at zero temperature are the variational Monte Carlo (VMC) and the diffusion Monte Carlo (DMC). In the VMC the variaitonal method is applied to approximate the ground state of the system. This results in $N$-dimensional integrals that are evaluated numerically through the use of a complex wave function, this is expaned upon in section~\ref{sec:theory} \cite{Kato57, Jastrow55}. The DMC uses a Green's function to solve the Schr\"{o}dinger equation. The method can find the exact ground-state energy within a given error for any quantum system though various techniques such as the implementation of a stochastic method \cite{Alser80}.

\section{Theory}\label{sec:theory}
According to the variational principle from established quatum theory the energy is given by
\begin{equation}\label{eq:energy}
E = \frac{\langle\Psi\mid H \mid\Psi\rangle}{\langle\Psi\mid\Psi\rangle}, \ \mathrm{where} \ E\ge E_G,
\end{equation}
where $\Psi$ is the variational wavefunction, $H$ is the Hamiltonian of the system in question, and $E_G$ is the ground state energy of the system. The Hamiltonian of a hydrogen molecule using the Born-Oppenheimer approximation assume that the nuclear motion of the particles is negligible and is thus a composition of the two electrons' potential and kinetic eneries in addition to their interactions. The Hamiltonian is given as
\begin{equation}\label{eq:H_total}
H = H_1 + H_2 + H_{ee},
\end{equation}
where the term of \eqref{eq:H_total} as described below.
\begin{eqnarray}
& H_1 = -\dfrac{1}{2}\nabla_1^2 - \dfrac{1}{r_{1L}} -\dfrac{1}{r_{1R}}\\
& H_2 = -\dfrac{1}{2}\nabla_1^2 - \dfrac{1}{r_{2L}} -\dfrac{1}{r_{2R}}\\
& H_{ee} = \dfrac{1}{r_{12}}
\end{eqnarray}
where $H_1$ is the contribution from the first electron, $H_2$ is the contribution from the second electron, and $H_{ee}$ is the contribution from the electron-electron interaction. Each position $r_{1L}$, $r_{1R}$, $r_{2L}$, and $r_{2R}$ are each electron's position with respect to the proton's position and $r_{12}$ is the relative position of one electron to the other. The following conventions are employed for the above values
\begin{eqnarray}
& \vec{r}_{1L} = \vec{r}_1 + \frac{s}{2}\hat{i}; \ \vec{r}_{1R} = \vec{r}_1 - \frac{s}{2}\hat{i},\\
%& \vec{r}_{1R} = \vec{r}_1 - \frac{s}{2}\hat{i} \\
& \vec{r}_{2L} = \vec{r}_2 + \frac{s}{2}\hat{i}; \ \vec{r}_{2R} = \vec{r}_2 - \frac{s}{2}\hat{i}, \\
%& \vec{r}_{2R} = \vec{r}_2 - \frac{s}{2}\hat{i} \\
& \vec{r}_{12} = \vec{r}_1 - \vec{r}_2, 
\end{eqnarray}
where $s$ is the proton's position along the $x$-axis. Given the above, the variational wave function is chosen to be
\begin{equation}\label{eq:variational-wave}
\Psi\left(\vec{r}_1,\vec{r}_2\right) = \phi\left(\vec{r}_1\right)\phi\left(\vec{r}_2\right)\psi\left(\vec{r}_1,\vec{r}_2\right),
\end{equation}
where $\phi\left(\vec{r}_1\right)$ is defined as 
\begin{equation}\label{eq:phi-1}
\phi\left(\vec{r}_1\right) = e^{-r_{1L}/a} + e^{-r_{1R}/a} = \phi_{1L} + \phi_{1R},
\end{equation}
$\phi\left(\vec{r}_2\right)$ is defined as
\begin{equation}\label{eq:phi-2}
\phi\left(\vec{r}_2\right) = e^{-r_{2L}/a} + e^{-r_{2R}/a} = \phi_{2L} + \phi_{2R},
\end{equation}
and $\psi\left(\vec{r}_1,\vec{r}_2\right)$ is given by the Jastrow function:
\begin{equation}\label{eq:jastrow}
\psi\left(\vec{r}_1,\vec{r}_2\right) = e^{\dfrac{\mid\vec{r}_1-\vec{r}_2\mid}{\alpha\left(1 + \beta\mid\vec{r}_1-\vec{r}_2\mid\right)}}
\end{equation}
\cite{Jastrow55}. Introducing the weight, $\omega$ defined as
\begin{equation}\label{eq:weight}
\omega\left(\vec{r}_1,\vec{r}_2,s\right) = \dfrac{\Psi^2\left(\vec{r}_1,\vec{r}_2,s\right)}{\langle\Psi\mid\Psi\rangle},
\end{equation}
and the local energy, $\epsilon$ defined as
\begin{equation}\label{eq:local-e}
\epsilon\left(\vec{r}_1,\vec{r}_2,s\right) = \dfrac{H\Psi\left(\vec{r}_1,\vec{r}_2,s\right)}{\Psi\left(\vec{r}_1,\vec{r}_2,s\right)}.
\end{equation}
$\epsilon$ can be transformed into the following (see \cite{Duxbury14} for a derivation)
\begin{equation}\label{eq:epsilon}
\epsilon = -\dfrac{1}{a^2} + \chi - R + \xi\eta - \zeta + \frac{1}{s},
\end{equation}
%where $\chi$ is defined by Eq.\eqref{eq:chi}, $R$ is defined by Eq.\eqref{eq:R}, $\xi$ is given as Eq.\eqref{eq:xi}, $\eta$ as Eq.\eqref{eq:eta}, and $\zeta$ as Eq.\eqref{eq:zeta}.
where $\chi$ is given as
\begin{equation}\label{eq:chi}
\chi = \dfrac{1}{a\phi_1}\left(\dfrac{\phi_{1L}}{r_{1L}}+\dfrac{\phi_{1R}}{r_{1R}}\right) + \dfrac{1}{a\phi_2}\left(\dfrac{\phi_{2L}}{r_{2L}}+\dfrac{\phi_{2R}}{r_{2R}}\right),
\end{equation}
$R$ is given as
\begin{equation}\label{eq:R}
R = \left[\dfrac{1}{r_{1L}}+\dfrac{1}{r_{1R}}+\dfrac{1}{r_{2L}}+\dfrac{1}{r_{2R}}\right] - \dfrac{1}{\mid r_{12}\mid},
\end{equation}
\begin{equation}\label{eq:xi}
\xi = \dfrac{\phi_{1L}\hat{r}_{1L} + \phi_{1R}\hat{r}_{1R}}{\phi_1} - \dfrac{\phi_{2L}\hat{r}_{2L} + \phi_{2R}\hat{r}_{2R}}{\phi_2}
\end{equation}
$\eta$ is given as 
\begin{equation}\label{eq:eta}
\eta = \dfrac{\hat{r}_{12}}{2a\left(1 + \beta r_{12}\right)^2},
\end{equation}
$\zeta$ is given as
\begin{equation}\label{eq:zeta}
\zeta = \dfrac{\left(4\beta+1\right)r_{12} + 4}{4\left(1 + \beta r_{12}\right)^4 r_{12}},
\end{equation}
and the $1/s$ term is representative of the proton-proton interaction in the normalized unit system. \eqref{eq:weight} and \eqref{eq:epsilon} combine to form the total energy equation
\begin{equation}\label{eq:big-E}
E = \int\int d^3r_1 \ d^3r_2 \ \omega\left(\vec{r}_1,\vec{r}_2,s\right)\epsilon\left(\vec{r}_1,\vec{r}_2,s\right).
\end{equation}

There are four variational parameters; $s$, $a$, $\alpha$, and $\beta$. Two of which can be removed using the Coulomb cusp conditions which ensure that there is no singularity in the energy when an electron approaches either proton or in the case that the electrons occupy the same position \cite{Kato57, Jastrow55}. The electron approaching the proton singularity case leads the condition
\begin{equation}\label{eq:cusp}
a\left(1+e^{-s/a}\right) = 1.
\end{equation}
On the other hand the singularity arising from the two electron approaching each other yields the condition that $\alpha = 2$ \cite{Duxbury14}.
\bibliographystyle{plain}
\bibliography{bibs/refs}

\end{document}
%
% ****** End of file apssamp.tex ******

